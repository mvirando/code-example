const profile = {
  type: 'tenant / service provider / landlord',
  rating: 5,
  email: 'hello@mvirando.com',
  name: 'Martin Virando',
  telephone: 07717664487,
  _id: 123456765432,
  documents: [
    { type: 'rental-agreement', link: '' },
    { type: 'deposit-certificate', link: '' },
    { type: 'check-in-inventory', link: '' },
    { type: 'other-documents', link: '' }
  ],
  current_property: 12212121212,
  notifications: [],
  moving_out: new Date(),
  // If you are a landlord / estate agent:
  properties: [
    12212121212
  ],
  quotations: {
    no_automatic: true,
    agree_all: false,
    agree_below: 50
  }
};

const property = {
  _id: 12212121212,
  address: {
    line1: '18 Macduff Road',
    line2: null,
    town: null.
    city: 'London',
    post_code: 'SW11 4DA'
  },
  issues: {
    common: [],
    existing: []
  },
  housemates: [123456765432],
  wifi: {
    name: 'Virgin Media',
    password: 'Battersea Park',
    location: 'Outside the basement',
    instructions: '',
    contact: 01235613123,
    account_number: 212112112,
    account_name: 'John Doe'
  },
  instructions: [
    {
      name: 'TV Instructions',
      link: ''
    }
  ],
  service_providers: [
  ] 
}