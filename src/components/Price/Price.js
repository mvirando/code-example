import React, { PropTypes } from 'react';
import './Price.scss';

const Price = ({
  value,
  showTitle = true,
  isMinus
}) => {
  return (
    <div className="price">
      {showTitle && <p className="price__title">Price:</p>}
      <p className="price__value">
        <span style={{color: isMinus ? 'red' : 'green' }}>
          {isMinus ? '-' : ''}{`£${value.toFixed(2)}`}
        </span>
      </p>
    </div>
  );
};

Price.propTypes = {
  value: PropTypes.number,
  showTitle: PropTypes.bool,
  isMinus: PropTypes.bool
};

export default Price;