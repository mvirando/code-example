import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Price from './Price';

describe('<Price />', () => {

  it('should format the price', () => {
    const wrapper = shallow(<Price value={0.50} />);
    expect(wrapper.find('.price__value').text()).to.equal('£0.50');
  });

  it('should format the discount price', () => {
    const wrapper = shallow(<Price value={0.50} isMinus={true} />);
    expect(wrapper.find('.price__value').text()).to.equal('-£0.50');
  });

  it('should hide the title when not needed', () => {
    const wrapper = shallow(<Price value={0} showTitle={false} />);
    expect(wrapper.find('.price__title')).to.have.length(0);
  });

  it('should show the title', () => {
    const wrapper = shallow(<Price value={0} />);
    expect(wrapper.find('.price__title')).to.have.length(1);
  });

});