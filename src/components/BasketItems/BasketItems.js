import React, { PropTypes } from 'react';
import BasketItem from '../BasketItem/BasketItem';

const BasketItems = ({
  items,
  isSingleItem
}) => {
  return (
    <ul className="basket__items">
      {items.map((item, i) => <BasketItem item={item} isSingleItem={isSingleItem} key={i} />)}
    </ul>
  );
};

BasketItems.propTypes = {
  items: PropTypes.array,
  isSingleItem: PropTypes.bool
};

export default BasketItems;