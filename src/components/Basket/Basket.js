import React, {PropTypes} from 'react';
import Price from '../Price/Price';
import BasketItems from '../BasketItems/BasketItems';
import { _calculateTotal } from '../../utils';
import './Basket.scss';

const Basket = ({
  items,
  calculatedItems,
}) => {

  return (
    <div className="basket">
      <h1>Your Basket</h1>
      {items.length ? <BasketItems items={items} isSingleItem={true} /> : <p className="basket__empty">Basket is Empty</p>}
      {items.length ? <h4>Basket Breakdown: </h4> : null}
      <BasketItems items={calculatedItems} isSingleItem={false} />      
      <h3>Total:</h3>
      <Price value={_calculateTotal(calculatedItems)} showTitle={false} />
    </div>
  );
};

Basket.propTypes = {
  items: PropTypes.array,
  calculatedItems: PropTypes.array
};

export default Basket;
