import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import BasketItem from './BasketItem';

describe('<BasketItem />', () => {

  let item = {
    name: 'Product 1',
    amount: 2,
    price: 0.25,
    totalPrice: 0.50,
    discount: false,
    savings: 0.50
  };

  it('should render name as name with the (x amount)', () => {
    const wrapper = shallow(<BasketItem item={item} isSingleItem={true} />);
    expect(wrapper.find('.basket__name').text()).to.equal(item.name);
  });

  it('should render the name with it amount.', () => {
    const wrapper = shallow(<BasketItem item={item} isSingleItem={false} />);
    expect(wrapper.find('.basket__name').text()).to.equal(item.name + ' x ' + item.amount);
  });

  it('should hide the discount price and show the just the item price', () => {
    const wrapper = mount(<BasketItem item={item} isSingleItem={true} />);
    expect(wrapper.find('.price')).to.have.length(1);
    expect(wrapper.find('.price').text()).to.equal('£' + item.price.toFixed(2));
  });

  it('should show the discount price', () => {
    item.discount = true;
    const wrapper = mount(<BasketItem item={item} isSingleItem={false} />);
    expect(wrapper.find('.price')).to.have.length(2);
    expect(wrapper.find('.price').at(0).text()).to.equal('-£' + item.savings.toFixed(2));
  });

  it('should show the total price when not viewing as a single item', () => {
    const wrapper = mount(<BasketItem item={item} isSingleItem={false} />);
    expect(wrapper.find('.price').at(1).text()).to.equal('£' + item.totalPrice.toFixed(2));
  });

});