import React, {PropTypes} from 'react';
import Price from '../Price/Price';

const BasketItem = ({
  item,
  isSingleItem
}) => {

  const name = isSingleItem ? item.name : `${item.name} x ${item.amount}`;
  const { discount, savings, price, totalPrice, name: itemName } = item; 

  return (
    <li className="basket__item" key={itemName}>
      <p className="basket__name">{name}</p>
      {discount && savings && !isSingleItem && <Price value={savings} showTitle={false} isMinus={true}/>}
      <Price value={isSingleItem ? price : totalPrice} showTitle={false} />
    </li>
  );
};

BasketItem.propTypes = {
  item: PropTypes.object,
  isSingleItem: PropTypes.bool
};

export default BasketItem;