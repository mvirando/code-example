import React, { PropTypes } from 'react';
import Price from '../Price/Price';
import './ProductsList.scss';

const ProductsList = ({
  products,
  onProductClick
}) => {
  return (
    <ul className="products-list">
      {products.map(product => {
        const { name, discount, price } = product;
        return (
          <li className="products-list__item" key={name}>
            <div className="products-list__content">
              <h4 className="products-list__heading">{name} {discount && <em style={{color: 'red'}}>3 for 2 offer!</em>}</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt omnis corrupti ipsa alias, odit veniam.</p>
            </div>
            <div className="products-list__details">
              <Price value={price} showTitle={true} />
              <button className="button" onClick={() => onProductClick(product)}>Add To Basket</button>
            </div>
          </li>
        );
      })}
    </ul>
  );
};

ProductsList.propTypes = {
  products: PropTypes.array.isRequired,
  onProductClick: PropTypes.func.isRequired
};

export default ProductsList;
