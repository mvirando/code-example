import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import ProductsList from './ProductsList';

describe('<ProductsList />', () => {

  const products = [
    { name: 'Product-1', price: 0, discount: false },
    { name: 'Product-2', price: 0, discount: true }
  ];

  it ('should render the items', () => {
    const wrapper = shallow(<ProductsList products={products} />);
    expect(wrapper.find('.products-list__item').length).to.equal(2);
  });

  it ('should add product to cart on click', () => {    
    const onBtnClick = sinon.spy();
    const wrapper = shallow(<ProductsList products={products} onProductClick={onBtnClick} />);

    expect(onBtnClick.calledOnce).to.be.false;
    wrapper.find('.products-list__item').at(0).find('.button').simulate('click');
    expect(onBtnClick.calledWith(products[0])).to.be.true;

  });

});