import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import shopState from './shop.state';

const rootReducer = combineReducers({
  routing: routerReducer,
  shopState
});

export default rootReducer;