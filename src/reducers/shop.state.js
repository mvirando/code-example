import { _groupAndTotal } from '../utils';

const initialState = {
  items: [],
  calculatedBasket: []
};

export default function shopState(state = initialState, action) {
  let newState;
  switch(action.type) {
    case 'ADD_TO_BASKET': {

      newState = {...state};

      const newItem = {
        ...action.product,
        id: newState.items.length + 1
      };

      newState.items = [...newState.items, newItem];    
      newState.calculatedBasket = _groupAndTotal(newState.calculatedBasket, newItem);

      return newState;
    }
    default: {
      return state;
    }
  }
}
