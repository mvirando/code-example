export const addToBasket = (product) => ({
  type: 'ADD_TO_BASKET',
  product
});