import React, { PropTypes } from 'react';
import '../styles/styles.scss';

const App = ({
  children
}) => {
  return (
    <div className="app">
      {children}
    </div>
  );
};

App.propTypes = {
  children: PropTypes.array
};

export default App;