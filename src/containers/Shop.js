import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as shopActions from '../actions/shop.actions';
import { ProductsList, Basket } from '../components';

const Shop = ({
  shopState,
  actions
}) => {

  const { items, calculatedBasket } = shopState;

  const products = [
    { name: 'Apple', price: 0.25, discount: false },
    { name: 'Orange', price: 0.30, discount: false },
    { name: 'Garlic', price: 0.15, discount: false },
    { name: 'Papayas', price: 0.50, discount: true }
  ];

  return (
    <main className="page">
      <div className="page__content">
        <h1>Our {products.length} Products: </h1>
        <ProductsList
          products={products}
          onProductClick={actions.addToBasket}
        />
      </div>
      <aside className="page__sidebar">
        <Basket
          items={items}
          calculatedItems={calculatedBasket}
        />
      </aside>
    </main>
  );
};

const mapStateToProps = (state) => ({
  shopState: state.shopState
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(shopActions, dispatch)
});

Shop.propTypes = {
  shopState: PropTypes.object,
  actions: PropTypes.actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shop);
