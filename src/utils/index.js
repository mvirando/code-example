export const _groupAndTotal = (items, item) => {
  // Do not mutate...
  let newItems = [...items];
  // Find the index
  const index = newItems.findIndex(product => product.name === item.name);
  // If this does not exist in the basket previously.
  if (index === -1) {
    item.totalPrice = item.price;
    item.amount = 1;
    newItems.push(item);
  } else {
    let newItem = {
      ...newItems[index],
    };
    newItem.amount = newItem.amount + 1;
    newItem = _pricing(newItem);
    newItems[index] = newItem;
  }
  return newItems;
};

export const _pricing = (item) => {
  const { discount, amount, price } = item;
  if (discount && amount % 3 === 0) {
    const division = amount / 3;
    item.totalPrice = (amount - division) * price;
    item.savings = division * price;
  } else {
    item.totalPrice += price;
  }
  return item;
};

export const _calculateTotal = (items) => items.reduce((num, item) => num += item.totalPrice, 0);