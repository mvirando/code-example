import { expect } from 'chai';
import { _pricing, _calculateTotal, _groupAndTotal } from './';

describe('utils', () => {

  const items = [
    { name: 'Product-1', discount: true, amount: 3, totalPrice: 0.50, price: 0.25 },
    { name: 'Product-2', discount: false, amount: 3, totalPrice: 0.50, price: 0.25 },
    { name: 'Product-3', discount: false, amount: 3, totalPrice: 0.50, price: 0.25 },
    { name: 'Product-4', discount: false, amount: 3, totalPrice: 0.50, price: 0.25 }
  ];


  it ('_calculateTotal: should build the grand total from the items provided', () => {
    expect(_calculateTotal(items)).to.equal(2);
  });

  it ('_pricing: should calculate the price without discount', () => {
    const candidate = _pricing(items[1]);
    expect(candidate.totalPrice).to.equal(0.75);
    expect(candidate.savings).to.be.undefined;
  });

  it ('_pricing: should calculate the price with discount', () => {
    const candidate = _pricing(items[0]);
    expect(candidate.totalPrice).to.equal(0.50);
    expect(candidate.savings).to.equal(0.25);
  });

  it('_groupAndTotal: if item does not exist in array add it and give it a total price and an amount', () => {
    let item = { name: 'Product-2', discount: true, price: 0.25 };
    const groupedItems = [];
    const newItems =  _groupAndTotal(groupedItems, item);
    expect(newItems[0].totalPrice).to.equal(item.price);
    expect(newItems[0].amount).to.equal(1);
  });

  it('_groupAndTotal: if item does exists in array adjust the price and change the amount', () => {
    let item = { name: 'Product-1', discount: false, price: 0.25 };
    const groupedItems = [{ name: 'Product-1', discount: false, price: 0.25, amount: 1, totalPrice: 0.25 }];
    const newItems = _groupAndTotal(groupedItems, item);
    expect(newItems[0].amount).to.equal(2);
    expect(newItems[0].totalPrice).to.equal(0.50);
    expect(newItems[0].savings).to.be.undefined;
  });

  it('_groupAndTotal: if item does exists in array and it is discounted change the price and add the savings', () => {
    let item = { name: 'Product-1', discount: true, price: 0.25 };
    const groupedItems = [{ name: 'Product-1', discount: true, price: 0.25, amount: 2, totalPrice: 0.25 }];
    const newItems = _groupAndTotal(groupedItems, item);
    expect(newItems[0].amount).to.equal(3);
    expect(newItems[0].totalPrice).to.equal(0.50);
    expect(newItems[0].savings).to.equal(0.25);
  });

});